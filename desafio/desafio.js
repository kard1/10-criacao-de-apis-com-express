const express = require('express');
const path = require('path');

const axios = require('axios');

const app = express();

const getUsers = () => axios({
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/vdn.vtex.ds.v10=json'
    },
    url: 'https://api.vtex.com/acupula/dataentities/AL/search'
})


app.get('/', async (req, res) => {
    
    try {
        const { data: users } = await getUsers()

        if (users) {
            res.send(users)
        }

    } catch (error) {
        console.log(error)
    }
});



app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`server is running on port ${PORT}`));

var postData = {
  'method': 'POST',
  'hostname': 'api.vtex.com',
  'path': '/acupula/dataentities/AL/documents',
  'headers': {
    'Content-Type': 'application/json',
    'x-vtex-api-appKey': '{{X-VTEX-API-AppKey}}',
    'x-vtex-api-appToken': '{{X-VTEX-API-AppToken}}',
    'Accept': 'application/vnd.vtex.ds.v10+json'
  }
};

var req = http.request(postData, function (res) {
  var chunks = [];

  res.on("data", function (chunk) {
    chunks.push(chunk);
  });
  res.on("end", function (chunk) {
    var body = Buffer.concat(chunks);
    console.log(body.toString());
  });

  res.on("error", function (error) {
    console.error(error);
  });
});

var postData =  {
  nome:  "ted",
  idade:  23,
  email:  "tedmustang@hotmail.com",
  nascimento:  "1956-04-18",
  dataCriacao:  "2000-01-30T00:00:00",
}

req.write(JSON.stringify(postData));

req.end();
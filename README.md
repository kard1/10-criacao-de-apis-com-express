# Criação de APIs com Express

O Express é um framework para aplicativo da web do Node.js mínimo e flexível que fornece um conjunto robusto de recursos para aplicativos web e móvel.

https://expressjs.com/pt-br/

## Roteamento
O Roteamento refere-se à definição de terminais do aplicativo (URIs) e como eles respondem às solicitações do cliente. Para obter uma introdução a roteamento, consulte Roteamento básico.

O código a seguir é um exemplo de uma rota muito básica.

``` javascript
var express = require('express');
var app = express();

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
  res.send('hello world');
});
```

Métodos de roteamento
Um método de roteamento é derivado a partir de um dos métodos HTTP, e é anexado a uma instância da classe express.

o código a seguir é um exemplo de rotas para a raiz do aplicativo que estão definidas para os métodos GET e POST.

``` javascript
// GET method route
app.get('/', function (req, res) {
  res.send('GET request to the homepage');
});

// POST method route
app.post('/', function (req, res) {
  res.send('POST request to the homepage');
});
```

# Desafio

A partir disso, vocês criarão uma API que servirá como proxy entre uma das ferramentas que utilizamos, o Master Data. Ele é um banco de dados da plataforma com que trabalhamos e vocês farão uso dessa ferramenta para o desafio, SEMPRE seguindo as especificações da documentação.

O funcionamento básico deve ser vocês usarem o Postman para acessar a API de vocês e poderem enviar os parametros querystring e pelo corpo da requsição, criem `console.log` para poderem sempre assistir os resultados e tratativas que fizerem.

Vocês usarão o acronimo (acronym/entidade) "AL" e a conta (accountName) "acupula" para salvar os dados, essa entidade tem a seguinte configuração:

`Dica: No POST para o Master Data, vocês provavelmente precisarão utilizar o método JSON.stringfy(objeto) para enviar as informações para a API.`

```
nome: String // "nome"
idade: Int // 12
email: Email // "teste@teste.com"
nascimento: Date // "2000-01-30"
dataCriacao: DateTime // "2000-01-30T00:00:00"
```

Requisitos obrigatórios:
GET (método de busca) - https://documenter.getpostman.com/view/164907/masterdata-api-v102/2TqWsD?version=latest#8af01160-a283-46fd-adef-cdeb821e1dc1
POST (método de criação) - https://documenter.getpostman.com/view/164907/masterdata-api-v102/2TqWsD?version=latest#90b4a0ae-7aa5-4a68-90b4-77a7b3260e85

Para se destacar:
DELETE (método de deleção) - https://documenter.getpostman.com/view/164907/masterdata-api-v102/2TqWsD?version=latest#e4b6a65a-c808-4dd0-910c-91ee9414d8b3
PATCH (método de atualização) - https://documenter.getpostman.com/view/164907/masterdata-api-v102/2TqWsD?version=latest#7c1bba34-ff95-4447-9227-615f1c0c5130

Sigam a documentação e lembrem-se de organizar as pastas. É obrigatório a criação de uma pasta "desafio" nesse repositório e os scripts devem estar nele.